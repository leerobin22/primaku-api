import { Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginUserDto } from './dto/login-user.dto';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(loginUserDto: LoginUserDto): Promise<any> {
    const userDetail = await this.usersService.findOne({
      email: loginUserDto.email,
    });

    if (!userDetail) {
      throw new UnauthorizedException();
    }

    const passwordCheck = await bcrypt.compare(
      loginUserDto.password,
      userDetail.password,
    );

    if (!passwordCheck) {
      throw new UnauthorizedException();
    }

    const payload = {
      id: userDetail.id,
      email: userDetail.email,
      name: userDetail.name,
      role: userDetail.role,
    };
    return {
      access_token: await this.jwtService.signAsync(payload, {
        secret: `${process.env.SECRET}`,
      }),
    };
  }
}
