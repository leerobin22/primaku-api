import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UpdatePasswordDto } from './dto/update-password.dto';
const saltRounds = 10;

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async create(user: User, createUserDto: CreateUserDto): Promise<User> {
    const checkExistingUser = await this.userRepository.findOneBy({
      email: createUserDto.email,
    });
    if (checkExistingUser) {
      throw new BadRequestException('User already exist!');
    }

    if (user.role !== 'admin' && createUserDto.role === 'admin') {
      throw new BadRequestException('Unable to create Admin role!');
    }

    const passwordRegex =
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (!passwordRegex.test(createUserDto.password)) {
      throw new BadRequestException(
        'Password must be min 8 chars, contains uppercase, lowercase alphabets, number, and symbol',
      );
    }

    const hashedPassword = await bcrypt.hash(
      createUserDto.password,
      saltRounds,
    );
    createUserDto.password = hashedPassword;

    const newUser = this.userRepository.create({
      ...createUserDto,
    });

    return this.userRepository.save(newUser);
  }

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findOne(query: { id?: number; email?: string }): Promise<User> {
    if (!query || (!query.id && !query.email)) {
      throw new BadRequestException('Invalid parameters!');
    }

    const userDetail = await this.userRepository.findOneBy(query);
    if (!userDetail) throw new NotFoundException('User not found!');

    return userDetail;
  }

  async update(
    user: User,
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<User> {
    const userDetail: User = await this.findOne({ id });

    if (updateUserDto.email) {
      const checkExistingUser = await this.userRepository.findOneBy({
        email: updateUserDto.email,
      });
      if (checkExistingUser) {
        throw new BadRequestException('User already exist!');
      }

      userDetail.email = updateUserDto.email;
    }

    if (updateUserDto.role) {
      if (user.role !== 'admin' && updateUserDto.role === 'admin') {
        throw new BadRequestException('Unable to update role!');
      }

      userDetail.role = updateUserDto.role;
    }

    if (updateUserDto.name) {
      userDetail.name = updateUserDto.name;
    }

    return await this.userRepository.save(userDetail);
  }

  async updatePassword(
    user: User,
    updatePasswordDto: UpdatePasswordDto,
  ): Promise<User> {
    const passwordRegex =
      /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (!passwordRegex.test(updatePasswordDto.password)) {
      throw new BadRequestException(
        'Password must be min 8 chars, contains uppercase, lowercase alphabets, number, and symbol',
      );
    }

    const hashedPassword = await bcrypt.hash(
      updatePasswordDto.password,
      saltRounds,
    );
    user.password = hashedPassword;

    return await this.userRepository.save(user);
  }

  async remove(user: User, id: number) {
    if (user.role !== 'admin') {
      throw new BadRequestException(
        `You don't have permission to delete user!`,
      );
    }

    const checkUser = await this.userRepository.findOneBy({ id });
    if (!checkUser) {
      throw new NotFoundException('User not found!');
    }

    await this.userRepository.remove(checkUser);
    return { success: true };
  }
}
