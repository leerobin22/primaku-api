import { IsEmail, IsIn, IsNotEmpty, IsString, Matches } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsEmail()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsString()
  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/, {
    message:
      'Password must be min 8 chars, contains uppercase, lowercase alphabets, number, and symbol',
  })
  password: string;

  @IsNotEmpty()
  @IsIn(['admin', 'user'], { message: 'Invalid Role!' })
  @IsString()
  role: string;
}
