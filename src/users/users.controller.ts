import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Put,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from '../auth/auth.guard';
import { UpdatePasswordDto } from './dto/update-password.dto';

@Controller('api')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(AuthGuard)
  @Post('users')
  create(@Request() req: any, @Body() createUserDto: CreateUserDto) {
    return this.usersService.create(req.user, createUserDto);
  }

  @UseGuards(AuthGuard)
  @Get('users')
  findAll() {
    return this.usersService.findAll();
  }

  @UseGuards(AuthGuard)
  @Get('users/:id')
  findOne(@Param('id') id: number) {
    return this.usersService.findOne({ id });
  }

  @UseGuards(AuthGuard)
  @Patch('users/:id')
  update(
    @Request() req: any,
    @Param('id') id: number,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.update(req.user, id, updateUserDto);
  }

  @UseGuards(AuthGuard)
  @Put('password')
  updatePassword(
    @Request() req: any,
    @Body() updatePasswordDto: UpdatePasswordDto,
  ) {
    return this.usersService.updatePassword(req.user, updatePasswordDto);
  }

  @UseGuards(AuthGuard)
  @Delete('users/:id')
  remove(@Request() req: any, @Param('id') id: number) {
    return this.usersService.remove(req.user, +id);
  }
}
