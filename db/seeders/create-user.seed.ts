export const CreateUserSeed = {
  name: 'admin',
  email: process.env.ADMIN_EMAIL,
  password: process.env.ADMIN_PASS,
  role: 'admin'
}
