import { CreateUserSeed } from "db/seeders/create-user.seed"
import { MigrationInterface, QueryRunner } from "typeorm"
import dataSource from "db/data-source"
import * as bcrypt from 'bcrypt';

export class SeedUserTable1689824811137 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        CreateUserSeed.password = await bcrypt.hash(CreateUserSeed.password, 10)
        const user = await dataSource.getRepository('user').save(CreateUserSeed)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
